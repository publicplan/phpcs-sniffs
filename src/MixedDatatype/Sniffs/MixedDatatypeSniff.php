<?php
declare(strict_types=1);

namespace publicplan\PhpCsSniffs\MixedDatatype\Sniffs;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Util\Tokens;

/**
 * TBD
 */
final class MixedDatatypeSniff implements Sniff
{

  public function register(): array
  {
    return [T_CLASS];
  }

  public function process(File $file, $position): void
  {
    //$this->fixer = $file->fixer;
    $tokens = $file->getTokens();
    $next = $file->findNext(T_DOC_COMMENT_TAG,($position + 1));
    while ($next !== false) {
      if (($tokens[$next]['content'] === '@var'
          || $tokens[$next]['content'] === '@return'
          || $tokens[$next]['content'] === '@param')
        && isset($tokens[($next + 1)]) === true
        && $tokens[($next + 1)]['code'] === T_DOC_COMMENT_WHITESPACE
        && isset($tokens[($next + 2)]) === true
        && $tokens[($next + 2)]['code'] === T_DOC_COMMENT_STRING
        && substr($tokens[($next + 2)]['content'],0,5) == "mixed"
      ) {
        $error = 'Mixed Type found';
        $data  = [$tokens[$next]['content']];
        //xdebug_break();
        //$file->addFixableError($error, ($next + 2), self::class, $data);
        $file->addError($error, ($next + 2),'DataTypeNamespace', $data);
      }
      $next = $file->findNext(T_DOC_COMMENT_TAG,($next + 1));
    }
  }

}
?>
